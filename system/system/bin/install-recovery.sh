#!/system/bin/sh
if ! applypatch -c EMMC:/dev/block/platform/bootdevice/by-name/recovery:14271392:c328508699f02ea80390b7130b09157fb49ce592; then
  applypatch  EMMC:/dev/block/platform/bootdevice/by-name/boot:8977312:8748b14945c6a4ba45ad2ba851f2a984ecf8b49d EMMC:/dev/block/platform/bootdevice/by-name/recovery c328508699f02ea80390b7130b09157fb49ce592 14271392 8748b14945c6a4ba45ad2ba851f2a984ecf8b49d:/system/recovery-from-boot.p && log -t recovery "Installing new recovery image: succeeded" || log -t recovery "Installing new recovery image: failed"
else
  log -t recovery "Recovery image already installed"
fi
