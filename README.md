## full_k63v2_64_bsp-user 9 PPR1.180610.011 1588048551 release-keys
- Manufacturer: ulefone
- Platform: mt6763
- Codename: Armor_X5
- Brand: Ulefone
- Flavor: full_k63v2_64_bsp-user
- Release Version: 9
- Id: PPR1.180610.011
- Incremental: 1588048551
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: false
- Locale: en-US
- Screen Density: 320
- Fingerprint: Ulefone/Armor_X5/Armor_X5:9/PPR1.180610.011/1588048551:user/release-keys
- OTA version: 
- Branch: full_k63v2_64_bsp-user-9-PPR1.180610.011-1588048551-release-keys
- Repo: ulefone_armor_x5_dump_6032


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
